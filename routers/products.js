const express = require('express');
const products = express.Router();
const response = require('../helpers/responses')
const { getProducts, createProduct } = require('../controllers/products')

//Swagger get Product
/**
 * @swagger
 * /products:
 *   get:
 *     summary: product fetcher
 *     tags: [products]
 *     responses:
 *       200:
 *         description: product fetched
 *       400:
 *         description: failed to fetch product
 */

products.route('/').get(async (req, res) => {
    try {
        const result = await getProducts();
        response.success(result, "product fetched!", res)	
    } catch (error) {
        response.error({ error: err.message }, req.originalUrl, 400, res)
    }
})

//Swagger create Product
/**
 * @swagger
 * /products:
 *   post:
 *     summary: product fetcher
 *     tags: [products]
 *     responses:
 *       200:
 *         description: product created
 *       400:
 *         description: failed to create product
 */

products.route('/').post(async (req, res) => {
    try {
        const result = await createProduct(req.body);
        response.success(result, "product created!", res) 
    } catch (error) {
        response.error({ error: err.message }, req.originalUrl, 400, res)
    }

})

module.exports = products
