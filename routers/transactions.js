const express = require('express')
const transactions = express.Router()
const response = require('../helpers/responses')
const { randomOrderNumber } = require('../helpers/utils')
const { addTransactions, fetchTransaction } = require('../controllers/transactions')

//Swagger post Transactions
/**
 * @swagger
 * /transactions:
 *   post:
 *     summary: transactions fetcher
 *     tags: [transactions]
 *     responses:
 *       200:
 *         description: new transaction created
 *       400:
 *         description: failed to created new transaction
 */

transactions.route('/').post(async(req, res) => {
    const  {total_price, paid_amount, products} = req.body

    const order = {
        no_order: randomOrderNumber(),
        total_price, 
        paid_amount
    }

    try {
        const result = await addTransactions(order, products)
        response.success(result, "new transaction created!", res)
    } 
    catch (err) {
        response.error({ error: err.message }, req.originalUrl, 400, res)
    }
})


//Swagger get Transactions
/**
 * @swagger
 * /transactions:
 *   get:
 *     summary: transactions fetcher
 *     tags: [transactions]
 *     responses:
 *       200:
 *         description: transactions fetched
 *       400:
 *         description: failed to fetch transactions
 */

transactions.route('/').get(async(req, res) => {
    try {
        const result = await fetchTransaction();
        response.success(result, "transaction fetched", res)
    } catch (err) {
        response.error({error: err.message}, req.originalUrl, 400, res)
    }
})

module.exports = transactions