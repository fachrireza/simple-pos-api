exports.randomOrderNumber = () => {
    const prefix = 'T'
    const randomNumber = Math.floor(Math.random() * 1000)
    return prefix + randomNumber
}

exports.timestampFormat = (date) => {
    const formattedDate = `${date.getFullYear()}-${(date.getMonth() + 1)
        .toString()
        .padStart(2, '0')}-${date
            .getDate()
            .toString()
            .padStart(2, '0')} ${date
                .getHours()
                .toString()
            .padStart(2, '0')}:${date
                    .getMinutes()
                    .toString()
            .padStart(2, '0')}:${date.getSeconds().toString().padStart(2, '0')}`;
    return formattedDate
}