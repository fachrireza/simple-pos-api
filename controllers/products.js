const db = require('../configs/connection')

exports.getProducts = async () =>{
    const query = await db.query("SELECT * FROM PRODUCTS")
    return query
}

exports.createProduct = async (data) => {
    const query = await db.query('INSERT INTO PRODUCTS SET ?', [data])
    return { id: query.insertId }
}