const express = require('express')
const swaggerJsdoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");
const bodyParser = require('body-parser')
const cors = require('cors')

const options = {
    definition: {
        swagger: "2.0",
        title: "LEARNING API DOCS",
        openapi: "3.1.0",
        info: {
            title: "LEARNING API DOCS",
            description: "POS-SERVER API Documentation"
        },
        schemes: ["dev-sandbox"]
    },
    apis: ["./routers/*.js"],
};

const app = express()
const port = 8080

const specs = swaggerJsdoc(options);
app.use(
    "/docs",
    swaggerUi.serve,
    swaggerUi.setup(specs, { explorer: true })
);

app.use(bodyParser.json())
app.use(cors())
app.get('/', (req, res) => {
    res.send("API Ready To GO!");
})

const products = require('./routers/products')
const transactions = require('./routers/transactions')
app.use('/products', products)
app.use('/transactions', transactions)


app.listen(port, () => {
    console.log(`Server running on port ${port}`)
})
